package com.sonarsource.interview;

import static com.sonarsource.interview.ArithmeticOperation.ADD;
import static com.sonarsource.interview.ArithmeticOperation.DIVIDE_BY;
import static com.sonarsource.interview.ArithmeticOperation.MULTIPLY_BY;
import static com.sonarsource.interview.ArithmeticOperation.SUBSTRACT;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ArithmeticOperationNumberExtractionTest {
	
	@Rule
    public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void extract_positive_number() {
		assertThat(ADD.extractNumber("ADD 5")).isEqualTo(5);
		assertThat(SUBSTRACT.extractNumber("SUBSTRACT 5")).isEqualTo(5);
		assertThat(MULTIPLY_BY.extractNumber("MULTIPLY BY 5")).isEqualTo(5);
		assertThat(DIVIDE_BY.extractNumber("DIVIDE BY 5")).isEqualTo(5);
	}
	
	@Test
	public void extract_negative_number() {
		assertThat(ADD.extractNumber("ADD -5")).isEqualTo(-5);
		assertThat(SUBSTRACT.extractNumber("SUBSTRACT -5")).isEqualTo(-5);
		assertThat(MULTIPLY_BY.extractNumber("MULTIPLY BY -5")).isEqualTo(-5);
		assertThat(DIVIDE_BY.extractNumber("DIVIDE BY -5")).isEqualTo(-5);
	}
	
	@Test
	public void extract_number_with_leading_spaces() {
		assertThat(ADD.extractNumber("ADD     -5")).isEqualTo(-5);
		assertThat(ADD.extractNumber("ADD     5")).isEqualTo(5);
	}
	
	@Test
	public void extract_number_with_trailing_spaces() {
		assertThat(ADD.extractNumber("ADD -5    ")).isEqualTo(-5);
	}
	
	@Test
	public void extract_min_max_integer() {
		assertThat(ADD.extractNumber("ADD " + Integer.MAX_VALUE)).isEqualTo(Integer.MAX_VALUE);
		assertThat(ADD.extractNumber("ADD " + Integer.MIN_VALUE)).isEqualTo(Integer.MIN_VALUE);
	}
	
	@Test
	public void throws_exception_when_extraction_has_no_number() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Not a valid number");
		ADD.extractNumber("ADD ");
	}
	
	
	@Test
	public void check_isOperation_not_case_sensitive() {
		assertThat(ADD.isOperation("ADD " + Integer.MAX_VALUE)).isTrue();
		assertThat(DIVIDE_BY.isOperation("DiViDe by " + Integer.MAX_VALUE)).isTrue();
	}

}
