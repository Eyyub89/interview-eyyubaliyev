package com.sonarsource.interview;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;

public class CalculatorTest {

	private static final String BASIC_CASE = "ADD 5\n" + 
					"SUBSTRACT 3\n" + 
					"MULTIPLY BY 2\n" + 
					"DIVIDE BY 2\n" + 
					"DISPLAY\n";

	@Test
	public void test_string_is_null() {
		assertThat(Calculator.of(null).calculate()).hasSize(0);
	}
	
	@Test
	public void test_string_is_empty() {
		assertThat(Calculator.of("").calculate()).hasSize(0);
	}
	
	@Test
	public void test_basic_case() {
		String input = BASIC_CASE;
		List<Double> result = Calculator.of(input).calculate();
		assertThat(result).hasSize(1);
		assertThat(result).contains(2d);
	}
	
	@Test
	public void test_basic_case_with_3_displays() {
		String input = BASIC_CASE +
						"DISPLAY\n" +
						"DISPLAY\n";
		List<Double> result = Calculator.of(input).calculate();
		assertThat(result).hasSize(3);
	}
	
	@Test
	public void test_floating_number() {
		String input = BASIC_CASE +
				"DIVIDE BY -4\n" +
				"DISPLAY\n" +
				"ADD 5\n" +
				"DISPLAY\n"
				;
		List<Double> result = Calculator.of(input).calculate();
		assertThat(result).hasSize(3);
		assertThat(result.get(2)).isEqualTo(4.5);
	}

	@Test
	public void test_10_displays() {
		String input = "DISPLAY\n" + 
						"DISPLAY\n" + 
						"DISPLAY\n" + 
						"DISPLAY\n" + 
						"DISPLAY\n" + 
						"DISPLAY\n" + 
						"DISPLAY\n" + 
						"DISPLAY\n" + 
						"DISPLAY\n" + 
						"DISPLAY\n";
		List<Double> result = Calculator.of(input).calculate();
		assertThat(result).hasSize(10);
	}
}
