package com.sonarsource.interview;

import static com.sonarsource.interview.ArithmeticOperation.ADD;
import static com.sonarsource.interview.ArithmeticOperation.DIVIDE_BY;
import static com.sonarsource.interview.ArithmeticOperation.MULTIPLY_BY;
import static com.sonarsource.interview.ArithmeticOperation.SUBSTRACT;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ArithmeticOperationTest {
	
	private static final String DIVIDE_BY_OPERATION = "DIVIDE BY 6";
	private static final String MULTIPLY_BY_OPERATION = "MULTIPLY BY 6";
	private static final String SUBSTRACT_OPERATION = "SUBSTRACT 6";
	private static final String ADD_OPERATION = "ADD 6";
	@Rule
    public ExpectedException expectedException = ExpectedException.none();
	
	
	@Test
	public void check_isOperation_not_case_sensitive() {
		assertThat(ADD.isOperation("ADD " + Integer.MAX_VALUE)).isTrue();
		assertThat(DIVIDE_BY.isOperation("DiViDe by " + Integer.MAX_VALUE)).isTrue();
	}
	
	@Test
	public void calculate_addition() {
		assertThat(ADD.calculate(1, ADD_OPERATION)).isEqualTo(7);
	}
	
	@Test
	public void calculate_substraction() {
		assertThat(SUBSTRACT.calculate(1, SUBSTRACT_OPERATION)).isEqualTo(-5);
	}
	
	@Test
	public void calculate_multiplication() {
		assertThat(MULTIPLY_BY.calculate(3, MULTIPLY_BY_OPERATION)).isEqualTo(18);
	}
	
	@Test
	public void calculate_division() {
		assertThat(DIVIDE_BY.calculate(3, DIVIDE_BY_OPERATION)).isEqualTo(0.5);
	}

	@Test
	public void calculate_division_by_zero() {
		expectedException.expect(ArithmeticException.class);
		DIVIDE_BY.calculate(3, "DIVIDE BY 0");
	}
	
	@Test
	public void getOperation_succefully() {
		assertThat(ArithmeticOperation.getOperation(ADD_OPERATION)).isEqualTo(ADD);
		assertThat(ArithmeticOperation.getOperation(SUBSTRACT_OPERATION)).isEqualTo(SUBSTRACT);
		assertThat(ArithmeticOperation.getOperation(MULTIPLY_BY_OPERATION)).isEqualTo(MULTIPLY_BY);
		assertThat(ArithmeticOperation.getOperation(DIVIDE_BY_OPERATION)).isEqualTo(DIVIDE_BY);
	}
	
	@Test
	public void throw_exception_on_getOperation() {
		this.expectedException.expect(IllegalArgumentException.class);
		ArithmeticOperation.getOperation("sdfdsf");
	}

}
