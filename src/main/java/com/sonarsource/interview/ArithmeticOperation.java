package com.sonarsource.interview;

import java.util.function.DoubleBinaryOperator;

public enum ArithmeticOperation {
	ADD("ADD", (left, right) -> left+right),
	SUBSTRACT("SUBSTRACT", (left, right) -> left-right),
	MULTIPLY_BY("MULTIPLY BY", (left, right) -> left*right),
	DIVIDE_BY("DIVIDE BY", (left, right) -> {
		if (right == 0d) {
			throw new ArithmeticException("by zero");
		}
		return left/right;	
	});
	
	private String name;
	private DoubleBinaryOperator action;
	
	private ArithmeticOperation(String name, DoubleBinaryOperator action) {
		this.name = name;
		this.action = action;
	}
	
	static ArithmeticOperation getOperation(String line) {
		for(ArithmeticOperation operation: ArithmeticOperation.values()) {
			if (operation.isOperation(line)) {
				return operation;
			}
		}
		
		throw new IllegalArgumentException("Not a valid operation");
	}
	
	Double calculate(double left, String line) {
		Double right = extractNumber(line); 
		return action.applyAsDouble(left, right);
	}
	
	Double extractNumber(String line) {
		int index = this.name.length()+1;
		if (line.length() <= index) {
			throw new IllegalArgumentException("Not a valid number");
		}
		return Double.valueOf(line.substring(index));
	}
	

	boolean isOperation(String line) {
		return line.toUpperCase().startsWith(name);
	}
}
