package com.sonarsource.interview;

import java.util.ArrayList;
import java.util.List;

public class Calculator {
	private static final String DISPLAY = "DISPLAY";
	
	private String[] input;
	private List<Double> state;
	private double lastValue;
	
	
	private Calculator(String input) {
		this.input = input == null || input.isEmpty()? new String[0]: input.split("\\n");
		this.state = new ArrayList<>();
		this.lastValue = 0d;
	}
	
	public static Calculator of(String input) {
		return new Calculator(input);
	}
	
	public List<Double> calculate(){
		for(String line: input) {
			processLine(line.trim());
		}
		
		return state;
	}

	private void processLine(String line) {
		if (isDisplayCommand(line)) {
			processDisplayOperation();
		}
		else {
			processArithmeticalOperation(line);
		}
	}

	private boolean isDisplayCommand(String line) {
		return DISPLAY.equalsIgnoreCase(line);
	}

	private void processDisplayOperation() {
		this.state.add(this.lastValue);
	}

	private void processArithmeticalOperation(String line) {
		ArithmeticOperation operation = ArithmeticOperation.getOperation(line);
		this.lastValue = operation.calculate(this.lastValue, line);
	}
}
